package EventsAndObservers;

public interface PersonObserver 
{
	public void personUpdate(Object o, int eventType, int objectLocation, int personID);
}
