package EventsAndObservers;

public interface Observer    
{
	public void update(Object o, int eventType, int objectLocation);
}
