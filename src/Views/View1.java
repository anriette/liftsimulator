package Views;

import EventsAndObservers.Observer;
import EventsAndObservers.PersonObserver;

import Model.Button;
import Model.Door;
import Model.Lift;
import Model.Person;

public class View1 implements Observer, PersonObserver
{	
	private static final String GROUND_FLOOR_NAME = "the ground floor";
	private static final String FIRST_FLOOR_NAME = "the first floor";

	public View1()
	{		
		
	}
   	
    public void update(Object o, int eventType, int objectLocation)   
    {
    		if (o instanceof Button)
    		{
    			switch (eventType) 
    			{
    				case 0: eventType = 0; 
    					if (objectLocation == 0)
    						System.out.println("Button on " + GROUND_FLOOR_NAME + " has been pressed.");
    					else if (objectLocation == 1)
    						System.out.println("Button on " + FIRST_FLOOR_NAME + " has been pressed.");
    					break;
    				case 1: eventType = 1; 
    					if (objectLocation == 0)
    						System.out.println("Button on " + GROUND_FLOOR_NAME + " has been reset.");
    					else if (objectLocation == 1)
    						System.out.println("Button on " + FIRST_FLOOR_NAME + " has been reset.");
					break;
    			}
    		}
    		else if (o instanceof Door)
    		{
    			switch (eventType) 
    			{
    				case 0: eventType = 0; 
					if (objectLocation == 0)
						System.out.println("Doors opening on " + GROUND_FLOOR_NAME + ".");
					else if (objectLocation == 1)
						System.out.println("Doors opening on " + FIRST_FLOOR_NAME + ".");
					break;
    				case 1: eventType = 1; 
    					if (objectLocation == 0 )
    						System.out.println("Doors closing on " + GROUND_FLOOR_NAME + ".");
    					else if (objectLocation == 1)
    						System.out.println("Doors closing on " + FIRST_FLOOR_NAME + ".");
					break;
    			}
    		}
    		else if (o instanceof Lift)
    		{
    			switch (eventType) 
    			{
    				case 0: eventType = 0; 
    					if (objectLocation == 0) 
    						System.out.println("Lift is on " + GROUND_FLOOR_NAME + ".");
    					else if (objectLocation == 1) 
    						System.out.println("Lift is on " + FIRST_FLOOR_NAME + ".");
    					break;
    				case 1: eventType = 1; 
					System.out.println("Lift is travelling between floors.");
					break;
    			}
    		}
    }    
    
    public void personUpdate(Object o, int eventType, int objectLocation, int personID)
    {
	    if (o instanceof Person)
		{
			switch (eventType) 	
			{
				case 0: eventType = 0; 
					if (objectLocation == 0)
						System.out.printf( "Person with ID %d is created on " + GROUND_FLOOR_NAME + ".\n", personID);
					if (objectLocation == 1)
						System.out.printf( "Person with ID %d is created on " + FIRST_FLOOR_NAME + ".\n", personID);
					break;
				case 1: eventType = 1; 
					if (objectLocation == 0)
					System.out.printf( "Person with ID %d pressed the button on " + GROUND_FLOOR_NAME + ".\n", personID);
				if (objectLocation == 1)
					System.out.printf( "Person with ID %d pressed the button on " + FIRST_FLOOR_NAME + ".\n", personID);
					break;
				case 2: eventType = 2;	
				System.out.printf("Person with ID %d is getting into the lift.\n", ((Person) o).getPersonID());
					break;
				case 3: eventType = 3;	
					if (objectLocation == 0)
					System.out.printf( "Person with ID %d is getting out of the lift on " + GROUND_FLOOR_NAME + ".\n", personID);
				if (objectLocation == 1)
					System.out.printf( "Person with ID %d is getting out of the lift on " + FIRST_FLOOR_NAME + ".\n", personID);
				break;
			}
		}
    }

}
