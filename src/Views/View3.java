package Views;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import EventsAndObservers.Observer;
import EventsAndObservers.PersonObserver;
import Model.Button;
import Model.Door;
import Model.Lift;
import Model.Person;

public class View3 extends JPanel implements Observer, PersonObserver
{
	//Button
	private int groundFloorButton = 1;
	private int firstFloorButton = 1;

	private Color grey = new Color (238, 238, 238);
	private Color pink = new Color (230, 0, 126);
	
    //Door
	int yDoorGroundFloor = 237;
	int yDoorFirstFloor = 37;
	private int xDoorGroundFloorPosition = 225;	// initialises doors on top of lift
    private int xDoorFirstFloorPosition = 225;	
    private int doorDirection = 2;
    private int doorOpenPosition = 325;
    private int closedDoorPosition = 225;
    private int doorWidth = 160;
    
	private Color doorGrey = new Color (181, 181, 181);
	
	private Timer doorTimer; // timer for Door

	//Person
    private String walkImageName = "walk";  
	private ImageIcon walkImg[]; 
	private int noOfPersonImages = 10;
	private int currentPersonImage = 0;
	
	private int yPersonGroundFloor = 244;
	private int yPersonFirstFloor = 44;
    private int yPersonPosition;
    private int xPersonPosition = -94;	// initialises person as just outside frame
    private int personDirection = 2;
    private int personOutsideFrame = -94;
    private int buttonPosition = 203;
	private int personInLiftPosition = 322;

    private int personCounter = 0;	//counts people; provides IDs
    
	private Timer personTimer; // timer for Person

	public View3() 	
	{		
		this.setBackground(Color.BLACK); // creates contrast between what is the simulation view and what is not, in case the simulation window is enlarged 
		
		// gets images for Person 
		walkImg = new ImageIcon[noOfPersonImages];
		for (int i = 0; i < walkImg.length; i++)	
		{
			walkImg[i] = new ImageIcon(getClass().getResource("resources/" + walkImageName + i + ".gif"));
		}
	}	
	
	
	
	// Methods
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g); 
		
		g.setColor(grey);
		g.fillRect(0, 0, 550, 400);
		
		g.setColor(new Color (85, 85, 85));
		g.fillRect(0, 197, 550, 3);	// draws upper floor and lift 
		g.fillRect(225, 37, 100, 160);
		g.fillRect(0, 397, 550, 3);	// draws lower floor and lift  
		g.fillRect(225, 237, 100, 160);
		
		// updates Button colour
		if (firstFloorButton == 1)
			g.fillOval(192, 105, 16, 16);
		if (groundFloorButton == 1)
			g.fillOval(192, 305, 16, 16);

		g.setColor(pink);
		if (firstFloorButton == 0)
			g.fillOval(192, 105, 16, 16);
		if (groundFloorButton == 0)
			g.fillOval(192, 305, 16, 16);

		g.setColor(doorGrey);
		// draws Door if open (so that it is shown under the Person)
		if (xDoorGroundFloorPosition >= doorOpenPosition)
			g.fillRect(doorOpenPosition, yDoorGroundFloor, 100, doorWidth);
		if (xDoorFirstFloorPosition >= doorOpenPosition)
			g.fillRect(doorOpenPosition, yDoorFirstFloor, 100, doorWidth);
		
		// updates Person position and image 
        walkImg[currentPersonImage].paintIcon(this, g, xPersonPosition, yPersonPosition);
		currentPersonImage = (currentPersonImage + 1) % noOfPersonImages;

		// draws Door if door is not fully open (so that it is shown over the Person)
		if (xDoorGroundFloorPosition < doorOpenPosition)
			g.fillRect(xDoorGroundFloorPosition, yDoorGroundFloor, 100, doorWidth);
		if (xDoorFirstFloorPosition < doorOpenPosition)
			g.fillRect(xDoorFirstFloorPosition, yDoorFirstFloor, 100, doorWidth);
	}
	
	public void update(Object o, int eventType, int objectLocation)
	{
		if (o instanceof Button)
		{
			switch (eventType) 
			{
				case 0: eventType = 0;
					if (objectLocation == 0)
						groundFloorButton = 0;
					else if (objectLocation == 1)
						firstFloorButton = 0;
					break;
				case 1: eventType = 1; 	
					if (objectLocation == 0)
						groundFloorButton = 1;
					else if (objectLocation == 1)	
						firstFloorButton = 1;
					break;
			}
		}
		else if (o instanceof Door)
		{
			doorEvent(objectLocation);
		}
		else if (o instanceof Lift)
		{
			if (eventType == 0 && objectLocation == 0)	 
				groundFloorButton = 1;
			else if (eventType == 0 && objectLocation == 1)	
				firstFloorButton = 1;
		}
	}
	
	public void personUpdate(Object o, int eventType, int objectLocation, int personID)
	{
		if (o instanceof Person)
		{
			if (objectLocation == 0)
				yPersonPosition = yPersonGroundFloor;
			else if (objectLocation == 1)
				yPersonPosition = yPersonFirstFloor;
			
			switch (eventType) 	
			{
				case 0: eventType = 0;
					personCounter++;
					personEvent(eventType);
					break;
				case 2: eventType = 2;	
					personEvent(eventType);
					break;
				case 3: eventType = 3;	
					if (objectLocation == 0)
						yPersonPosition = yPersonGroundFloor;
					else if (objectLocation == 1)
						yPersonPosition = yPersonFirstFloor;
					personEvent(eventType);
					break;
			}
		}
	}
	
	public void doorEvent(int floor)		
	{
		if (doorTimer == null) 
		{
			currentPersonImage = 0; 
			doorTimer = new Timer(50, new TimerHandler()
				{
					public void actionPerformed(ActionEvent e) 		
					{
						if (floor == 0)
						{
							xDoorGroundFloorPosition += doorDirection;

							if (xDoorGroundFloorPosition >= doorOpenPosition) 
							{
								xDoorGroundFloorPosition = doorOpenPosition;
								doorDirection *= -1;
							}
							else if (xDoorGroundFloorPosition <= closedDoorPosition) 
							{
								doorDirection *= 0;
								xDoorGroundFloorPosition = closedDoorPosition;	// brings door to stand still
							} 
						}
						else if (floor == 1)
						{
							xDoorFirstFloorPosition += doorDirection;

							if (xDoorFirstFloorPosition >= doorOpenPosition) 
							{
								xDoorFirstFloorPosition = doorOpenPosition;
								doorDirection *= -1;
							}
							else if (xDoorFirstFloorPosition <= closedDoorPosition) 
							{
								doorDirection *= 0;
								xDoorFirstFloorPosition = closedDoorPosition;	// brings door to stand still
							} 
						}
						 
						repaint();
					}
				});
			doorTimer.start(); 
	      } 
		else 
		{
			if (!doorTimer.isRunning())	
				doorTimer.restart();
		}
	}
	
	public void personEvent(int eventType)		
	{
		if (personTimer == null) 
		{
			currentPersonImage = 0; 
			personTimer = new Timer(50, new TimerHandler()
				{
					public void actionPerformed(ActionEvent e) 
					{
						xPersonPosition += personDirection;
						
						if (eventType == 3)
						{
							if (xPersonPosition < personInLiftPosition) 
							{
								xPersonPosition = personInLiftPosition;
								personDirection *= -1;
							}
							else if (xPersonPosition + walkImg[currentPersonImage].getIconWidth() >= getWidth() + walkImg[currentPersonImage].getIconWidth()) 
							{
								personDirection *= 0;
								currentPersonImage = 0;	// brings person to stand still
								stopAnimation();
							} 
						}
						else if (eventType == 2)
						{
							if (xPersonPosition < buttonPosition) 
							{
								xPersonPosition = buttonPosition;
								personDirection *= -1;
							}
							else if (xPersonPosition + walkImg[currentPersonImage].getIconWidth() >= personInLiftPosition) 
							{
								personDirection *= 0;
								currentPersonImage = 0;	// brings person to stand still
							}
						}
						else if (eventType == 0)
						{
							if (xPersonPosition < personOutsideFrame) 
							{
								xPersonPosition = personOutsideFrame;
								personDirection *= -1;
							}
							else if (xPersonPosition + walkImg[currentPersonImage].getIconWidth() >= buttonPosition) 
							{
								personDirection *= 0;
								currentPersonImage = 0;	// brings person to stand still
							} 
						}
						 
						repaint();
					}
				});
			personTimer.start(); 
	      } 
		else 
		{
			if (!personTimer.isRunning())
			personTimer.restart();
		}
	}
	
	public void stopAnimation()
	{
		personTimer.stop();
	}


	
	private class TimerHandler implements ActionListener 
	{
		public void actionPerformed( ActionEvent actionEvent )
		{
			repaint();
		} 
	} 	
	
}
