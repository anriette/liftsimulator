package Views;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;

public class View2 extends JPanel 
{
    private JButton personOnGroundFloorButton; 
    private JButton personOnFirstFloorButton; 	

	public View2() 	
	{		
		personOnGroundFloorButton = new JButton("Ground Floor");
		personOnGroundFloorButton.setToolTipText("Adds a person to the ground floor");
		add(personOnGroundFloorButton);

		personOnFirstFloorButton = new JButton("First Floor");
		personOnFirstFloorButton.setToolTipText("Adds a person to the first floor");
		add(personOnFirstFloorButton);
	}
	
	
	
	// Methods
	public void addPersonListener(ActionListener listenerForPerson)
	{
		getPersonOnGroundFloorButton().addActionListener(listenerForPerson);
		getPersonOnFirstFloorButton().addActionListener(listenerForPerson);
	}



	public JButton getPersonOnGroundFloorButton() {
		return personOnGroundFloorButton;
	}
	
	public JButton getPersonOnFirstFloorButton() {
		return personOnFirstFloorButton;
	}
	
}
