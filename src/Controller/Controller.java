package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.LiftModel;
import Views.View1;
import Views.View2;
import Views.View3;

public class Controller 
{
	private LiftModel liftModel;
	private View1 GUI1;
	private View2 GUI2;
	private View3 GUI3;
	
	public Controller (LiftModel aModel, View1 view1, View2 view2, View3 view3) 
	{
		liftModel = aModel;
		GUI1 = view1;
		GUI2 = view2;
		GUI3 = view3;
		
		GUI2.addPersonListener (new PersonListener());
	}
	
	
	
	class PersonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) 	// checks what button produced the event, and creates a person on the corresponding floor.
		{
			if (e.getSource() == GUI2.getPersonOnGroundFloorButton())
				liftModel.addPerson(0);
			else if (e.getSource() == GUI2.getPersonOnFirstFloorButton())
				liftModel.addPerson(1);
		}		
	}
	
}
