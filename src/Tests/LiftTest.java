package Tests;

import javax.swing.JFrame;

import Controller.Controller;
import Model.LiftModel;
import Views.View1;
import Views.View2;
import Views.View3;

public interface LiftTest 
{
	public static void main(String [] args) 
	{
		View1 GUI1 = new View1();
		View2 GUI2 = new View2();
		View3 GUI3 = new View3();

		LiftModel theModel = new LiftModel(GUI1, GUI3);
		
		Controller LiftController = new Controller(theModel, GUI1, GUI2, GUI3);		

		JFrame liftAnimation = new JFrame("Lift Simulator 2000");
		liftAnimation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		liftAnimation.add(GUI3);
		liftAnimation.setSize(550, 422);
		liftAnimation.setVisible(true);
		
		JFrame createPersons = new JFrame("Add People");
		createPersons.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		createPersons.add(GUI2);
		createPersons.pack();
		createPersons.setVisible(true);
	}

}

