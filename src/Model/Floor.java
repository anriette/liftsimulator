package Model;

import EventsAndObservers.Observer;
import EventsAndObservers.PersonObserver;

import Model.Door;
import Model.Button;

import Views.View1;
import Views.View3;

public class Floor implements Observer, PersonObserver
{
	private int floorNo;
	private int noOfPeopleWaiting = 0;
	private Button floorButton;
	private Door floorDoor;
	private Door synchDoor;

	public Floor (int floorNumber, View1 GUI1, View3 GUI3, Lift theLift, Door synchronizeDoor) 		
	{
		floorNo = floorNumber;
		floorButton = new Button (floorNo, GUI1, GUI3);	
		floorDoor = new Door (floorNo, GUI1, GUI3);	
		synchDoor = synchronizeDoor;
		theLift.registerObserver(this);
	}
	
	
	
	//Methods 
	public int getFloorNo() 
	{
		return floorNo;
	}
	
	public int getNoOfPeopleWaiting() 
	{
		return noOfPeopleWaiting;
	}
	
	public void addPersonToFloor()
	{
		noOfPeopleWaiting++;
	}
	
	public void removePersonFromFloor(int floor)
	{
		if (noOfPeopleWaiting > 0)	
			noOfPeopleWaiting--;
	}
	
	public void updateButtonStatus(int status)
	{
		floorButton.setButtonStatus(status);	// View is updated on Button being pressed  
	}
	
	
	
	// Observer related
	public void update(Object o, int eventType, int objectLocation)
	{		
		if (o instanceof Lift)
		{
			switch (eventType) 	
    			{
    				case 0: eventType = 0; 
    					if (objectLocation == floorNo)
    					{	
    						floorDoor.setDoorStatus(0);
    						if (floorButton.getButtonStatus() == 0 && getNoOfPeopleWaiting() > 0)		// View is only updated when someone has pressed the button on the new floor
    							updateButtonStatus(1); 	 
    					}
					break;
    				case 1: eventType = 1; 
    					if (objectLocation == floorNo)	
    					{
	    					if (floorDoor.getDoorStatus() == 0)	
	    						floorDoor.setDoorStatus(1);
    					}
    					break;
    				case 2: eventType = 2;
    					if (objectLocation == floorNo && floorDoor.getDoorStatus() == 0)
    					{
    						try 
		    				{
		    					Thread.sleep(1000);	// Lift stays open one second per person entering
		    				} 
		    				catch (InterruptedException e) 
		    				{

		    				}

    						if (getNoOfPeopleWaiting() == 0)	 	
    							floorDoor.setDoorStatus(1);
    					}
    					break;
    			}
    		}	
	}
	
	public void personUpdate(Object o, int eventType, int objectLocation, int personID)
	{
		if (o instanceof Person)
		{
			switch (eventType) 	
	    		{
	    			case 0: eventType = 0; 	
	    				if (objectLocation == floorNo)
	    					addPersonToFloor();	
					break;
	    			case 1: eventType = 1; 
		    			if (objectLocation == floorNo)
					{
    						if (floorButton.getButtonStatus() == 1)
    							updateButtonStatus(0);
			    			if (synchDoor.getDoorFloor() != floorNo)	// sends lift to opposite floor if no one is waiting on floor  
			    				synchDoor.setDoorFloor(floorNo);
			    			else if (synchDoor.getDoorFloor() == floorNo && floorDoor.getDoorStatus() != 0) // opens closed doors if lift is on same floor		 	
			    				floorDoor.setDoorStatus(0);
		    			}
	    				break;	
	    			case 2: eventType = 2;
	    				if (synchDoor.getDoorFloor() == floorNo && objectLocation == synchDoor.getDoorFloor() && noOfPeopleWaiting > 0)	
	    				{	
    						removePersonFromFloor(floorNo);
	    					 if (noOfPeopleWaiting == 0)
	    						synchDoor.updateFloor(floorNo);
		    			}
		    			break;
    			}
    		}
	}

}


