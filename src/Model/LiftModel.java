package Model;

import Model.Door;
import Model.Floor;
import Model.Lift;
import Model.Person;

import Views.View1;
import Views.View3;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class LiftModel 
{
	private int personCounter = 0; // generates person IDs

	private View1 GUI1;
	private View3 GUI3;
	private Door theDoor;
	private Floor GroundFloor;
	private Floor FirstFloor;	
	private Lift theLift; 
    ExecutorService liftExecutor = Executors.newCachedThreadPool();

	public LiftModel(View1 view1, View3 view3) 
	{
		GUI1 = view1;
		GUI3 = view3;
		theDoor = new Door (0, GUI1, GUI3);	// door used by Lift and Person(s) threads to synchronise movement	
		theLift = new Lift (0, GUI1, GUI3, theDoor);	
		GroundFloor = new Floor (0, GUI1, GUI3, theLift, theDoor);
		FirstFloor = new Floor (1, GUI1, GUI3, theLift, theDoor);
		Thread lift = new Thread(theLift);	// starts Lift
		lift.start();
	}
	
	
	
	//Methods
	public void addPerson(int floor)
	{
		Thread person = new Thread(new Person(personCounter, floor, GUI1, GUI3, theLift, theDoor, GroundFloor, FirstFloor));
		person.start();
		personCounter++;	 	// updates to create new ID for next person 
	}
	
}
