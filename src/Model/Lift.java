package Model;

import java.util.*;

import EventsAndObservers.Observer;
import EventsAndObservers.PersonObserver;

import Model.Subject;

import Views.View1;
import Views.View3;

public class Lift implements Runnable, Subject, PersonObserver
{
	
	private int liftLocation;	
	private int liftStatus; // 0 = arrives on a floor, 1 = traveling between floors , 2 = remains on a floor
	private int noOfPassengers;
	private int maxNoOfPassengers = 5;
	private boolean liftOnDoorFloor = false;
	private Door door;
	private ArrayList<Observer> liftObservers;
		
	public Lift (int currentLocation, View1 GUI1, View3 GUI3, Door theDoor) 	
	{
		liftLocation = currentLocation;
		liftStatus = 0; 		// initialises lift as on a floor 
		door = theDoor;
		liftObservers = new ArrayList<Observer>();
		this.registerObserver(GUI1);
		this.registerObserver(GUI3);
	}
	
	
		
	public int getLiftLocation()
	{
		return liftLocation;
	}
	
	public void setLiftLocation(int floor)
	{
		this.liftLocation = floor;
	}
	
	public int getLiftStatus()
	{
		return liftStatus;
	}
	
	public void setLiftStatus(int status)
	{
		this.liftStatus = status;
		notifyObservers();
	}
	
	public int getMaxNoOfPassengers()
	{
		return maxNoOfPassengers;
	}
	
	public int getNoOfPassengers()
	{
		return noOfPassengers;
	}
	
	synchronized public void addPassenger()
	{
		noOfPassengers++;
	}
	
	synchronized public void removePassenger()
	{
		if (noOfPassengers >= 0)
			noOfPassengers--;
	}

	
	
	// Methods
	public void run() 
	{	
		while (1 != 0)	// makes sure the lift keeps running, as long as the simulation does
		{
			if (door.getDoorFloor() == getLiftLocation() && noOfPassengers < maxNoOfPassengers)	// Lift is on same floor as synchronisation door and has allowed number of passengers
			{
				setLiftStatus(2); 			
			}
			else 
			{
				liftOnDoorFloor = false;
				door.updateDoorStatus(liftOnDoorFloor);	 
								
				setLiftStatus(1); 				
				
				try
				{
					Thread.sleep(5000);		// the time it takes for the lift to travel between floors
				}
				catch (InterruptedException e)
				{
					
				}
				
				if (noOfPassengers >= maxNoOfPassengers)
					setLiftLocation(updateFloor(liftLocation));	// the lift moves itself, when it's full
				else
					setLiftLocation(door.getDoorFloor());
				
				setLiftStatus(0); 			
			}
			
			liftOnDoorFloor = true;
			door.updateDoorStatus(liftOnDoorFloor);	
		}	
	}
	
	public int updateFloor(int oldFloor)		//updates lift location to the opposite floor 
	{
		int newFloor = -1;		
		if (oldFloor == 0)	 
			newFloor = 1;	
		else 
			newFloor = 0;			
		return newFloor;
	}	
	
	
	
	//Observer related
	public void personUpdate(Object o, int eventType, int objectLocation, int personID)
	{
		if (o instanceof Person)
		switch (eventType)
		{
			case 2:
				addPassenger();
				break;
			case 3:
				removePassenger();
				break;
		}
	}
		
	
	
	//Subject related
	public void registerObserver(Observer lO) 
	{
		liftObservers.add(lO);
	}
	
	public void removeObserver(Observer lO) 
	{
		int i = liftObservers.indexOf(lO);
		if (i >= 0) 
		{
			liftObservers.remove(i);
		}
	}
	
	public void notifyObservers() 
	{
		for (int i = 0; i < liftObservers.size(); i++) 
		{
			Observer liftObserver = (Observer)liftObservers.get(i);
			liftObserver.update(this, liftStatus, liftLocation);
		}
	}
	
}
