package Model;

import EventsAndObservers.Observer;

import Model.Subject;

import Views.View1;
import Views.View3;

import java.util.*;

public class Button implements Subject
{	
	private int buttonStatus = 1;	//0 = pressed, 1 = reset	
	private int buttonFloor;
	private ArrayList<Observer> buttonObservers; 
	
	public Button (int floor, View1 GUI1, View3 GUI3)	
	{
		buttonFloor = floor;
		buttonObservers = new ArrayList<Observer>();
		this.registerObserver(GUI1);	
		this.registerObserver(GUI3);
	}
	
	
	
	public int getButtonFloor() 
	{
		return buttonFloor;
	}
	
	public void setButtonFloor(int floor)
	{
		this.buttonFloor = floor;
	}	
	
	public int getButtonStatus() 
	{
		return buttonStatus;
	}
	
	public void setButtonStatus(int status)
	{
		buttonStatus = status;
		notifyObservers();
	}
	
		
	
	// Subject related
	public void registerObserver(Observer bO) 
	{
		buttonObservers.add(bO);
	}
	
	public void removeObserver(Observer bO) 
	{
		int i = buttonObservers.indexOf(bO);
		if (i >= 0) 
		{
			buttonObservers.remove(i);
		}
	}
	
	public void notifyObservers() 
	{
		for (int i = 0; i < buttonObservers.size(); i++) 
		{
			Observer buttonObserver = (Observer)buttonObservers.get(i);
			buttonObserver.update(this, buttonStatus, buttonFloor);
		}
	}
	
}
