package Model;

import java.util.*;
import java.lang.Runnable;

import EventsAndObservers.PersonObserver;

import Model.Door;
import Model.Lift;

import Views.View1;
import Views.View3;

public class Person implements Runnable 
{
	private int personID;
	private int personStatus;	// 0 = creation, 1 = button pressed, 2 = into lift, 3 = out of lift
	private int personLocation; 	//0 = ground floor, 1 = first floor 
	private Door door;
	private Lift lift;    
	private ArrayList<PersonObserver> personObservers;	
	
	public Person (int anID, int aLocation, View1 GUI1, View3 GUI3, Lift theLift, Door theDoor, Floor GroundFloor, Floor FirstFloor)  
	{
		personID = anID;
		personLocation = aLocation;
		personStatus = 0;
		personObservers = new ArrayList<PersonObserver>();
		door = theDoor; 
		lift = theLift;	

		this.registerObserver(GUI1);		
		this.registerObserver(GUI3);
		this.registerObserver(theLift);
		this.registerObserver(GroundFloor);
		this.registerObserver(FirstFloor);
	}
	
	

	public int getPersonID()
	{
		return personID;
	}
	
	public int getPersonLocation()
	{
		return personLocation;
	}
	
	public void setPersonLocation(int location)
	{
		this.personLocation = location;
	}
	
	public int getLatestpersonStatus()
	{
		return personStatus;
	}

	public void setLatestpersonStatus(int eventType)
	{
		this.personStatus = eventType;
		notifyObservers();		
	}
	

	
	// Methods
	public void run() 
	{
		setLatestpersonStatus(0);	// notifies observers of creation of Person, to update view.
		setLatestpersonStatus(1);	// Person presses button on the floor where they have been created

		// Person travels with lift
		boolean doorAvailable = door.checkDoorStatus();
		
		while (doorAvailable != true || lift.getLiftLocation() != getPersonLocation())	// lift unavailable
		{
			door.checkDoorStatus();
			doorAvailable = door.checkDoorStatus();
		}
		
		setLatestpersonStatus(2);			// updates latest event for Person, to update View.

		setPersonLocation(updateFloor(personLocation));	// updates person location to new floor

		// Person leaves lift
		boolean newFloorDoorAvailable = door.checkDoorStatus();

		while (newFloorDoorAvailable != true || lift.getLiftLocation() != getPersonLocation())	// new floor not yet reached
		{
			door.checkDoorStatus();
		}
		
		setLatestpersonStatus(3);	
	}
	
	public int updateFloor(int oldFloor)		//updates person location to the opposite floor 
	{
		int newFloor = -1;		
		if (oldFloor == 0)	 
			newFloor = 1;	
		else 
			newFloor = 0;			
		return newFloor;
	}	

	
	
	// Subject related 
	public void registerObserver(PersonObserver pO) 
	{
		personObservers.add(pO);
	}
	
	public void removeObserver(PersonObserver pO)  
	{
		int i = personObservers.indexOf(pO);
		if (i >= 0) 
		{
			personObservers.remove(i);
		}
	}
	
	public void notifyObservers() 
	{
		for (int i = 0; i < personObservers.size(); i++) 
		{
			PersonObserver personObserver = (PersonObserver)personObservers.get(i);
			personObserver.personUpdate(this, personStatus, personLocation, personID);
			
		}		
	}
	
}
