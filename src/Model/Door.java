package Model;

import java.util.*;

import EventsAndObservers.Observer;

import Model.Subject;

import Views.View1;
import Views.View3;

public class Door implements Subject 
{
	private int doorStatus;	// 0 = open(ing), 1 = closed(-ing)
	private int doorFloor;
	private boolean liftAvailable = false;
	private boolean doorCanOpen = false;
	private ArrayList<Observer> doorObservers;
	
	public Door (int floor, View1 GUI1, View3 GUI3)	
	{
		doorStatus = 1; // initialises door as closed
		doorFloor = floor;
		doorObservers = new ArrayList<Observer>();
		this.registerObserver(GUI1);
		this.registerObserver(GUI3);
	}
	
	
	
	public int getDoorFloor() 
	{
		return doorFloor;
	}
	
	public void setDoorFloor(int floor)
	{
		this.doorFloor = floor;
	}
	
	public int getDoorStatus() 
	{
		return doorStatus;
	}
	
	public void setDoorStatus(int status)
	{
		this.doorStatus = status;
		notifyObservers();	
	}
	
	
	
	synchronized boolean checkDoorStatus()	// person checks lift availability
	{
		while (!liftAvailable)
		{
			try 
			{
				wait();	
			} 
			catch (InterruptedException e) 
			{
				
			}
		}
		
		liftAvailable = false;
	
		notifyAll();
		return doorCanOpen;		
	}
	
	synchronized public void updateDoorStatus(boolean aDoorStatus)	// lift updates lift availability
	{	
		while (liftAvailable)
		{
			try 
			{
				wait();
			} 
			catch (InterruptedException e) 
			{
				
			}	
		}
		
		doorCanOpen = aDoorStatus;	
		liftAvailable = true;
		notifyAll();		
	}
	
	public void updateFloor(int oldFloor)
	{
		int newFloor = -1;	//updates door location to the opposite floor 	
		if (oldFloor == 0)	 
			newFloor = 1;	
		else 
			newFloor = 0;			
		setDoorFloor(newFloor);
	}	
	
	
	
	//Subject related	
	public void registerObserver(Observer dO)
	{
		doorObservers.add(dO);
	}
	
	public void removeObserver(Observer dO)
	{
		int i = doorObservers.indexOf(dO);
		if (i >= 0) 
		{
			doorObservers.remove(i);
		}
	}
	
	public void notifyObservers()
	{
		for (Observer doorObserver : doorObservers)
		{
			doorObserver.update(this, doorStatus, doorFloor);
		}		
	}
	
}
